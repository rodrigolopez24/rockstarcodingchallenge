package rockstarcodingchallenge;

/**
 *
 * @author Rodrigo Lopez
 */
public class RockstarCodingChallenge {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Main object of the application - All the logic of the challenge is in the produceResults method.
        PersonsAnalyzer personsAnalyzer = new PersonsAnalyzer("resources/people.in");
        personsAnalyzer.produceResults();
    }
    
}
