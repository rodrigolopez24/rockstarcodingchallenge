package rockstarcodingchallenge.data.model;

/**
 *
 * @author Rodrigo Lopez
 */
public class LinkedinUser implements Comparable<LinkedinUser> {

    private Integer personId;

    private String name;

    private String lastName;

    private String currentRole;

    private String country;

    private String industry;

    private Integer numberOfRecommendations;

    private Integer numberOfConnections;

    private double scoreForBairesDev;

    public LinkedinUser(String line) {
        String[] fields = line.split("\\|");
        this.personId = Integer.parseInt(fields[0]);
        this.name = fields[1] != null && !fields[1].equals("") ? fields[1] : null;
        this.lastName = fields[2] != null && !fields[2].equals("") ? fields[2] : null;
        this.currentRole = fields[3] != null && !fields[3].equals("") ? fields[3] : null;
        this.country = fields[4] != null && !fields[4].equals("") ? fields[4] : null;
        this.industry = fields[5] != null && !fields[5].equals("") ? fields[5] : null;
        this.numberOfRecommendations = Integer.parseInt(fields[6]);
        this.numberOfConnections = Integer.parseInt(fields[7]);
        this.scoreForBairesDev = 0;
    }

    public Integer getPersonId() {
        return personId;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCurrentRole() {
        return currentRole;
    }

    public String getCountry() {
        return country;
    }

    public String getIndustry() {
        return industry;
    }

    public Integer getNumberOfRecommendations() {
        return numberOfRecommendations;
    }

    public Integer getNumberOfConnections() {
        return numberOfConnections;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public void setNumberOfRecommendations(Integer numberOfRecommendations) {
        this.numberOfRecommendations = numberOfRecommendations;
    }

    public void setNumberOfConnections(Integer numberOfConnections) {
        this.numberOfConnections = numberOfConnections;
    }

    public double getScoreForBairesDev() {
        return scoreForBairesDev;
    }

    public void setScoreForBairesDev(double scoreForBairesDev) {
        this.scoreForBairesDev = scoreForBairesDev;
    }

    @Override
    public String toString() {
        return "{" + "personId=" + personId + ", scoreForBairesDev=" + scoreForBairesDev
                + ", name=" + name + ", "
                + "lastName=" + lastName + ", currentRole=" + currentRole + ", country="
                + country + ", industry=" + industry + ", numberOfRecommendations="
                + numberOfRecommendations + ", numberOfConnections="
                + numberOfConnections + '}';
    }

    @Override
    public int compareTo(LinkedinUser o) {
        if (this.scoreForBairesDev > o.getScoreForBairesDev()) {
            return 1;
        } else if (this.scoreForBairesDev < o.getScoreForBairesDev()) {
            return -1;
        } else {
            return 0;
        }
    }

}
