package rockstarcodingchallenge.data.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Rodrigo Lopez
 */
public class Ranking {

    /**
     * According to the Harvard Business Review in their public article written
     * in 2019, these are the 42 countries with their ranking in their ease of
     * doing digital business.
     *
     * https://hbr.org/2019/09/ranking-42-countries-by-ease-of-doing-digital-business
     *
     *
     *
     * @return a Map containing the score of each country
     */
    public static Map<String, Double> getCountryRanking() {
        Map<String, Double> ranking = new HashMap<>();
        ranking.put("United States", 3.6);
        ranking.put("United Kingdom", 3.59);
        ranking.put("Netherlands", 3.41);
        ranking.put("Norway", 3.32);
        ranking.put("Japan", 3.27);
        ranking.put("Australia", 3.26);
        ranking.put("Denmark", 3.22);
        ranking.put("Switzerland", 3.21);
        ranking.put("Canada", 3.21);
        ranking.put("Finland", 3.21);
        ranking.put("Sweden", 3.2);
        ranking.put("New Zealand", 3.18);
        ranking.put("Singapore", 3.16);
        ranking.put("Germany", 3.11);
        ranking.put("Austria", 3.1);
        ranking.put("Estonia", 3.09);
        ranking.put("Ireland", 3.04);
        ranking.put("France", 3.01);
        ranking.put("Belgium", 2.99);
        ranking.put("Spain", 2.99);
        ranking.put("Portugal", 2.94);
        ranking.put("Italy", 2.88);
        ranking.put("Israel", 2.86);
        ranking.put("Korea", 2.86);
        ranking.put("Czech Republic", 2.83);
        ranking.put("Poland", 2.73);
        ranking.put("Chile", 2.66);
        ranking.put("Greece", 2.56);
        ranking.put("Hungary", 2.49);
        ranking.put("South Africa", 2.44);
        ranking.put("Mexico", 2.41);
        ranking.put("Brazil", 2.36);
        ranking.put("Thailand", 2.34);
        ranking.put("Philippines", 2.33);
        ranking.put("Colombia", 2.33);
        ranking.put("Malaysia", 2.32);
        ranking.put("Argentina", 2.27);
        ranking.put("India", 2.17);
        ranking.put("China", 2.14);
        ranking.put("Turkey", 2.02);
        ranking.put("Indonesia", 1.99);
        ranking.put("Russia", 1.96);
        return ranking;
    }

    /**
     * According to the Harvard Business Review in their public article written
     * in 2019, these are the top 10 business industries that are consuming 
     * the best tech skills in the market
     *
     * https://hbr.org/2019/05/ranking-countries-and-industries-by-tech-data-and-business-skills
     *
     *
     *
     * @return a Map containing the score of industry
     */
    public static Map<String, Double> getIndustryRanking() {
        Map<String, Double> ranking = new HashMap<>();
        ranking.put("Electrical/Electronic Manufacturing", 3.0);
        ranking.put("Manufacturing", 3.0);
        ranking.put("Insurance", 2.9);
        ranking.put("Telecommunications", 2.8);
        ranking.put("Biotechnology", 2.7);
        ranking.put("Information Technology and Services", 2.7);
        ranking.put("Financial Services", 2.6);
        ranking.put("Investment Banking", 2.6);
        ranking.put("Investment Management", 2.6);
        ranking.put("Banking", 2.6);
        ranking.put("Real Estate", 2.6);
        ranking.put("Brokerage", 2.6);
        ranking.put("Broadcast Media", 2.5);
        ranking.put("Online Media", 2.5);
        ranking.put("Broadcast Media", 2.5);
        ranking.put("Entertainment", 2.5);
        ranking.put("Health, Wellness and Fitness", 2.4);
        ranking.put("Hospital & Health Care", 2.4);
        ranking.put("Mental Health Care", 2.4);
        ranking.put("Medical Devices", 2.4);
        ranking.put("Management Consulting", 2.3);
        ranking.put("Automobiles", 2.2);
        ranking.put("Automotive", 2.2);
        ranking.put("Industrial Automation", 2.2);
        ranking.put("Airlines/Aviation", 2.2);
        ranking.put("Aviation & Aerospace", 2.2);
        ranking.put("Business Services", 2.2);
        ranking.put("Consumer Electronics", 2.1);
        ranking.put("Consumer Goods", 2.1);
        ranking.put("Consumer Services", 2.1);
        return ranking;
    }

}
