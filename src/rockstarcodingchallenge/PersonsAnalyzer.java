package rockstarcodingchallenge;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import rockstarcodingchallenge.data.model.LinkedinUser;
import rockstarcodingchallenge.data.model.Ranking;

/**
 *
 * @author Rodrigo Lopez
 */
public class PersonsAnalyzer {

    /**
     * The path were the provided information is located.
     */
    private final String inputFilePath;

    /**
     * The path were the output should be recorded, for simplicity is located
     * always in the same directory of the input.
     */
    private final String outputFilePath;

    public PersonsAnalyzer(String inputFilePath) {
        this.inputFilePath = inputFilePath;
        this.outputFilePath = "resources/people.out";
    }

    /**
     * This public method performs the required task of bringing the top 100
     * users the could be potential BairesDev clients.
     */
    public void produceResults() {
        List<LinkedinUser> users = this.readDataFromFile(this.inputFilePath);
        this.writeToFileTheTopNElements(users, 100);
    }

    /**
     * This method takes the input file, reads its content, and produce a list
     * with all the information.
     *
     * @param inputFilePath
     * @return null if there's a problem with the input file, otherwise it will
     * return a list of LinkedinUser entities with all the rows in the provided
     * example.
     */
    private List<LinkedinUser> readDataFromFile(String inputFilePath) {
        List<LinkedinUser> output = null;
        File input = new File(inputFilePath);
        Scanner scanner;
        try {
            scanner = new Scanner(input);
            output = new ArrayList<>();
            //Iterating over the file row by row
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                //The constructor of the LinkedinUser knows how to parse the
                //information for the given input format.
                LinkedinUser currentUser = new LinkedinUser(line);
                //The user score is calculated during this loop, to avoid performing
                //more iterations in the futre and reduce complexity.
                currentUser.setScoreForBairesDev(this.analyzeUserScore(currentUser));
                output.add(currentUser);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PersonsAnalyzer.class.getName()).log(Level.WARNING, null, ex);
        }
        return output;
    }

    /**
     *
     * @param input - The list of LinkedinUser with their calculated score
     * @param n - The number of results to retrive, from best match to worst
     */
    private void writeToFileTheTopNElements(List<LinkedinUser> input, int n) {
        if (input != null && input.size() >= n) {
            try (FileWriter fileWriter = new FileWriter(this.outputFilePath)) {
                Collections.sort(input, Collections.reverseOrder());
                Iterator iterator = input.iterator();
                for (int i = 0; i < n; i++) {
                    LinkedinUser lu = (LinkedinUser) iterator.next();
                    fileWriter.write(lu.getPersonId() + "\n");
                }
                fileWriter.close();
            } catch (IOException ex) {
                Logger.getLogger(PersonsAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    /**
     * This method is where the logic of the classification exists, it gives a
     * certain numeric score to all the users according to the highest chance of
     * them to becoming clients of BairesDev. If the number is higher, then the
     * changes are greater.
     *
     * @param u
     * @return the score of each user
     */
    private double analyzeUserScore(LinkedinUser u) {
        Map<String, Double> coruntryRankings = Ranking.getCountryRanking();
        Map<String, Double> industryRankings = Ranking.getIndustryRanking();
        double score = 0d;
        Double countryScore = -1d;
        Double industryScore = -1d;
        if (u.getPersonId() != null) {
            score++;
        }
        if (u.getCountry() != null) {
            countryScore = coruntryRankings.get(u.getCountry());
            countryScore = countryScore != null ? countryScore : 1.0;
        }
        if (u.getIndustry() != null) {
            industryScore = industryRankings.get(u.getIndustry());
            industryScore = industryScore != null ? industryScore : 1.0;
        }
        score += countryScore;
        score += industryScore;
        score += this.getRoleScore(u.getCurrentRole());
        score += this.getRecommendationScore(u.getNumberOfRecommendations());
        score += this.getNumberOfConnectionsScore(u.getNumberOfConnections());
        return score;
    }

    /**
     * This method produces output based on my perception of how some roles are
     * in charge of making decisions about hiring another company or not.
     *
     * @return a score for the provided role
     */
    private double getRoleScore(String role) {
        //The initial score is negative to eliminate noise in our universe of users.
        double score = -1d;
        if (role != null) {
            if (role.contains("acquisitions") || role.contains("strategies")
                    || role.contains("purchasing") || role.contains("purchase")
                    || role.contains("strategic") || role.contains("aquisition")
                    || role.contains("talent") || role.contains("human")
                    || role.contains("resources") || role.contains("capital")
                    || role.contains("business development") || role.contains("evaluation")
                    || role.contains("procurement") || role.contains("partnerships")) {
                score = 1.5d;
            } else if (role.contains("manager") || role.contains("administrator")
                    || role.contains("senior") || role.contains("head") || role.contains("regional")
                    || role.contains("recruiter") || role.contains("lead")) {
                score = 1d;
            } else if (role.contains("executive") || role.contains("president")
                    || role.contains("director") || role.contains("vp")) {
                score = .5d;
            }
        }
        return score;
    }

    /**
     *
     * The recommendation number does not provide enough information for the
     * analysis, that's why a very small variation exists if this field is
     * present, more analysis is required in this field.
     *
     * @param numberOfRecommendations
     * @return a score for the provided recommendation
     */
    private double getRecommendationScore(Integer numberOfRecommendations) {
        if (numberOfRecommendations != null && numberOfRecommendations > 0) {
            return 0.1d;
        }
        return 0d;
    }

    /**
     *
     * @param numberOfConnections
     * @return a score for the provided numberOfConnections
     */
    private double getNumberOfConnectionsScore(Integer numberOfConnections) {
        if (numberOfConnections != null) {
            return numberOfConnections / 250d;
        }
        return 0d;
    }
}
